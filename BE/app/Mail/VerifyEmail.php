<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->email    = $request->email;
        $this->name     = $request->name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = app()->staticUrl().'/api/active/'.Crypt::encryptString($this->email).'/'.Str::random(20);
        return $this->subject("Verify account")->markdown('emails.Verify', [ 'url' => $url, 'name' => $this->name ]);
    }
}
