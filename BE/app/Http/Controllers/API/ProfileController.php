<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\ProfileUpdateRequest;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use RuntimeException;
use App\Models\User;
use SebastianBergmann\Environment\Console;

/**
 * @group 7. User management
 */
class ProfileController extends Controller
{
    private $hash;

    public function __construct(Hash $hash)
    {
        $this->hash = $hash;
    }

    /**
     * Get current user's profile
     *
     * @response {
     *   "id": 42,
     *   "name": "John Doe",
     *   "email": "john@doe.com"
     * }
     *
     * @return JsonResponse
     */
    public function show(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Update current user's profile
     *
     * @bodyParam name string required New name. Example: Johny Doe
     * @bodyParam email string required New email. Example: johny@doe.com
     * @bodyParam password string New password (null/blank for no change)
     *
     * @response []
     *
     * @throws RuntimeException
     *
     * @return JsonResponse
     */
    public function update(ProfileUpdateRequest $request)
    {
        $data = $request->only('name', 'email', 'password');
        
        if ($request->password)
            if ($request->password != " ") 
                $data['password'] = $this->hash->make($request->password);
                else return response()->json([
                    'status'=>'FAIL',
                    'data' => '',
                    'message' => '',
                    ], 300);
        else return response()->json([
            'status'=>'FAIL',
            'data' => '',
            'message' => '',
            ], 300);

        return response()->json([
            'status'=>'OK',
            'data' => $request->user()->update($data),
            'message' => 'Success',
            ], 200);
    }
    
    /**
     * Update current user's  avatar
     *
     * @throws RuntimeException
     *
     * @return JsonResponse
     */
    public function imageUpload(Request $request)
    { 
        try{
        $location = 'public/img/avatar';
        if ($request->hasFile('fileImage')){
            $file = $request->fileImage;
           
            if($request->has('userId')) $id = $request->userId;
            else $id = substr($file->getClientOriginalName(), 0, strpos($file->getClientOriginalName(), '.'));
            
            $file_name = md5($id).".".$file->getClientOriginalExtension();
            $file->move($location, $file_name);
            $file_path = '../'.$location.'/'.$file_name;

            User::where('id', '=', $id)->update(array('avatar_path' => $file_path));
            return response()->json([
                'status'=>'OK',
                'data' => '',
                'message' => 'Success',
                ], 200);
        }
        else response()->json([
            'status'=>'Fail',
            'data' => '',
            'message' => 'Fail',
            ], 500);
        }catch(\Throwable $th){
            response(401); 
        }
    }
}
