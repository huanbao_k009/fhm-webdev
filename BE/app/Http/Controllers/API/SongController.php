<?php

namespace App\Http\Controllers\API;

use App\Factories\StreamerFactory;
use App\Http\Requests\API\SongPlayRequest;
use App\Http\Requests\API\SongUpdateRequest;
use App\Jobs\QStream;
use App\Models\Song;
use App\Models\Album;
use App\Models\Artist;
use App\Models\Interaction;
use App\Models\Setting;
use App\Repositories\AlbumRepository;
use App\Repositories\ArtistRepository;
use App\Services\MediaInformationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use App\Services\MediaCacheService;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

/**
 * @group 3. Song interactions
 */
class SongController extends Controller
{
    private $mediaInformationService;
    private $streamerFactory;
    private $artistRepository;
    private $albumRepository;
    private $mediaCacheService;

    public function __construct(
        MediaInformationService $mediaInformationService,
        StreamerFactory $streamerFactory,
        ArtistRepository $artistRepository,
        AlbumRepository $albumRepository,
        MediaCacheService $mediaCacheService
    ) {
        $this->mediaInformationService = $mediaInformationService;
        $this->streamerFactory = $streamerFactory;
        $this->artistRepository = $artistRepository;
        $this->albumRepository = $albumRepository;
        $this->mediaCacheService = $mediaCacheService;
    }

    /**
     * Play a song
     *
     * The GET request to play/stream a song. By default FHM will serve the file as-is, unless it's a FLAC.
     * If the value of `transcode` is truthy, FHM will attempt to transcode the file into `bitRate`kbps using ffmpeg.
     *
     * @response {}
     *
     * @queryParam jwt-token required The JWT token.
     *
     * @link https://github.com/phanan/fhm/wiki#streaming-music
     *
     * @param null|bool $transcode Whether to force transcoding the song.
     *                             If this is omitted, by default FHM will transcode FLAC.
     * @param null|int  $bitRate   The target bit rate to transcode, defaults to OUTPUT_BIT_RATE.
     *                             Only taken into account if $transcode is truthy.
     *
     * @return RedirectResponse|Redirector
     */
    public function play(SongPlayRequest $request, Song $song, ?bool $transcode = null, ?int $bitRate = null) {   
        //Bỏ task vô queue
        try {
            return $this->streamerFactory
            ->createStreamer($song, $transcode, $bitRate, floatval($request->time))
            ->stream();
            // QStream::dispatch($this->streamerFactory, $song, $transcode, $bitRate, $request);
        } catch (\Throwable $e) {
            // Log::channel('stderr')->info($this->streamerFactory
            // ->createStreamer($song, $transcode, $bitRate, floatval($request->time))
            // ->stream());
           dd($e);
        }
    }
    // thực hiện bất đồng bộ acync

    /**
     * Update song information
     *
     * @bodyParam songs array required An array of song IDs to be updated.
     * @bodyParam data object required The new data, with these supported fields: `title`, `artistName`, `albumName`, and `lyrics`.
     *
     * @group 5. Media information
     *
     * @return JsonResponse
     */
    public function update(SongUpdateRequest $request){
        $updatedSongs = Song::updateInfo($request->songs, $request->data);

        return response()->json([
            'artists' => $this->artistRepository->getByIds($updatedSongs->pluck('artist_id')->all()),
            'albums' => $this->albumRepository->getByIds($updatedSongs->pluck('album_id')->all()),
            'songs' => $updatedSongs,
        ]);
    }
    // GetSongs
    public function getSongs(){
        try {
            $songs_raw = Song::getSongs();
            $songs = [];
            foreach ($songs_raw as $key => $song_data) {
                if (Interaction::getPlayCountById($song_data['id'])['play_count'] == null) $play_count = 0;
                else $play_count = Interaction::getPlayCountById($song_data['id'])['play_count'];
                $songs[] = array(
                    'id'            =>  $song_data['id'],
                    'title'         =>  $song_data['title'],
                    'trackNumber'   =>  $song_data['track'],
                    'year'          =>  date('Y',strtotime($song_data['created_at'])),
                    'duration'      =>  $song_data['length']*1000,
                    'data'          =>  app()->staticUrl().'api/'.$song_data['id'].'/play?jwt-token=',
                    'dateModified'  =>  strtotime($song_data['updated_at']),
                    
                    'albumId'       =>  $song_data['album_id'],
                    'albumName'     =>  Album::getNameById($song_data['album_id'])['name'],
                    'albumArtPath'  =>  Album::getCoverById($song_data['album_id'])['cover'],
                    'artistId'      =>  $song_data['artist_id'],
                    'artistName'    =>  Artist::getNameById($song_data['artist_id'])['name'],
                    
                    'playCount'     =>  $play_count,
                    'lyric'         =>  $song_data['lyrics'],
                );
            }
            Log::channel('stderr')->info("Songs: ",$songs);
            return response()->json([
                'code'      => '200',
                'message'   => "Success",
                'data'      => $songs,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'code'      => '500',
                'message'   => $th,
            ]);
        }
    }

    /**
     * Update current user's  avatar
     *
     * @throws RuntimeException
     *
     * @return JsonResponse
     */
    public function upload(Request $request){       
        try{
        //Standard
            // if ($request->quantity != null && $request->quantity > 0){
            //     $quantity = $request->quantity;
            //     for ($i = 0; $i < $quantity; $i++) {
            //         if ($request->hasFile($i)){
            //             $location = Setting::get("media_path");
            //             $file = $request->$i;
            //             $file_name = $file->getClientOriginalName();
            //             $file->move($location, $file_name);
            //             $file_path = '../'.$location.'/'.$file_name;
        
            //             Artisan::call('fhm:sync');
            //             return response()->json([
            //                 'status'=>'OK',
            //                 'data' => $file_path,
            //                 'message' => 'Success',
            //                 ], 200);
            //         }
            //         else return $this->responseFail();
            //     }
            // }           
            // else return $this->responseFail();

            // return $this->responseFail();
        // -------------------------------------------------------------------------
            if($request->hasFile('content')) {
                if ($request->part < 10) $tempPath = 'chunks\#'.$request->name.'\_00'.$request->part.'.mp3';
                else if ($request->part < 100) $tempPath = 'chunks\#'.$request->name.'\_0'.$request->part.'.mp3';
                else $tempPath = 'chunks\#'.$request->name.'\_'.$request->part.'.mp3';
                
                Storage::put($tempPath, $request->content->get());
                if (count(Storage::allFiles('chunks\#'.$request->name)) == $request->total){
                    for ($i=0; $i < $request->total; $i++) { 
                        Storage::append($request->name, Storage::get(Storage::allFiles('chunks\#'.$request->name)[$i]));
                    }
                    file_put_contents(str_replace('#', '', Setting::get("media_path").'\#'.$request->name), Storage::get($request->name));
                    Storage::delete($request->name);
                    Storage::deleteDirectory('chunks\#'.$request->name);
                    Artisan::call('fhm:sync');
                    return $this->responseSuccess($request->file, $request->part, "Finish");
                }
                return $this->responseSuccess($request->file, $request->part, null);

            } else {
                // if (!$request->has('is_last'))
                // return $this->responseSuccess($request->file, $request->part, "Finish");
                return $this->responseFail($request->file, $request->part, null);
            }
               
        }catch(\Throwable $th){
            response(401); 
        }
    }

    private function responseFail($file = null, $data = null, $message = null){
        return response()->json([
            'status'=> 'Fail',
            'file' => $file == null ? "" : $file,
            'part' => $data == null ? "" : $data,
            'message' => $message == null ? "Fail" : $message,
        ]);
    }

    private function responseSuccess($file = null, $data = null, $message = null)    {
        return response()->json([
            'status'=>"OK",
            'file' => $file == null ? "" : $file,
            'part' => $data == null ? "" : $data,
            'message' => $message == null ? "Success" : $message,
        ]);
    }
}
