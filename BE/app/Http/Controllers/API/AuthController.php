<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\UserLoginRequest;
use App\Http\Requests\API\UserSigninRequest;
use App\Http\Requests\API\UserForgotPassRequest;
use Exception;
use App\Models\User;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Log\Logger;
use Illuminate\Support\Str;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Crypt;

/**
 * @group 1. Authentication
 */
class AuthController extends Controller
{
    private $auth;
    private $logger;
    private $hash;

    public function __construct(JWTAuth $auth, Logger $logger, Hash $hash)
    {
        $this->auth = $auth;
        $this->logger = $logger;
        $this->hash = $hash;
    }

    /**
     * Log a user in
     *
     * FHM uses [JSON Web Tokens](https://jwt.io/) (JWT) for authentication.
     * After the user has been authenticated, a random "token" will be returned.
     * This token should then be saved in a local storage and used as an `Authorization: Bearer` header
     * for consecutive calls.
     *
     * Notice: The token is valid for a week, after that the user will need to log in again.
     *
     * @bodyParam email string required The user's email. Example: john@doe.com
     * @bodyParam password string required The password. Example: SoSecureMuchW0w
     *
     * @response {
     *   "token": "<a-random-string>"
     * }
     * @reponse 401 {
     *   "message": "Invalid credentials"
     * }
     *
     * @return JsonResponse
     */
    public function login(UserLoginRequest $request)
    {
        $user = User::where('email', '=', $request->email)->where('active', '=', 1)->count();
        if ($user){
            $token = $this->auth->attempt($request->only('email', 'password'));
            abort_unless($token, 401, 'Invalid credentials');
        }   
        else abort_unless($user, 401, 'Invalid credentials');
        return response()->json([
            'status'=>'OK',
            'data'=>[
                    'token'=>$token,
                    'user'=>$request->user()
                ]
            ], 200);
    }

    /** 
    *
    * @throws RuntimeException
    *
    * @return JsonResponse
    */
    public function signin(UserSigninRequest $request)
    {
        Mail::to($request->email)->send(new \App\Mail\VerifyEmail($request)); 

        $response = response()->json(User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $this->hash->make($request->password),
            'active' => '0',
        ]));

        return response()->json([
            'status'=>'OK',
            'data' => $response,
            'message' => 'Success',
            ], 200);
    }

    /** 
    *
    * @throws RuntimeException
    *
    * @return JsonResponse
    */
    public function active(string $hash, string $rd)
    {
        $email = Crypt::decryptString($hash);
        $user = User::where('email', '=', $email)->update(array('active' => 1));
        if ($user) return Redirect::to(app()->staticUrl());
        else return response($user, 500);
    }

    /** 
    *
    * @throws RuntimeException
    *
    * @return JsonResponse
    */
    public function forgot_pass(UserForgotPassRequest $request)
    {
        $user = User::where('email', '=', $request->email)->count();
        if ($user != 0) {
            Mail::to($request->email)->send(new \App\Mail\ForgotPassword($request->email)); 
            return response()->json([
                'status'=>'OK',
                'data' => '',
                'message' => 'Success',
                ], 200);
        }
        else return response()->json([
                'status'=>'Err',
                'data' => '',
                'message' => 'User not exist',
                ], 402);
    }

    /** 
    *
    * @throws RuntimeException
    *
    * @return JsonResponse
    */
    public function reset_pass(string $hash, string $rd)
    {
        $email = Crypt::decryptString($hash);
        $newpass = Str::random(8);
        Mail::to($email)->send(new \App\Mail\ConfirmPassword($newpass));
        $user = User::where('email', '=', $email)->update(array('password' => $this->hash->make($newpass)));
        if ($user) return Redirect::to(app()->staticUrl());
        else return response($user, 300);
    }

    /**
     * Log the current user out
     *
     * @return JsonResponse
     */
    public function logout()
    {
        // sleep(500000);
        if ($token = $this->auth->getToken()) {
            try {
                $this->auth->invalidate($token);
            } catch (Exception $e) {
                $this->logger->error($e);
            }
        }

        // return response()->json();
        return response()->json([
            'status'=>'OK',
            'data' => '',
            ], 200);
    }
}
