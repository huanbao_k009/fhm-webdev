<?php

namespace App\Http\Requests\API;

class UserSigninRequest extends Request
{
    public function rules(): array
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ];
    }
}
