<?php

namespace App\Http\Requests\API;

class UserForgotPassRequest extends Request
{
    public function rules(): array
    {
        return [
            'email' => 'required|email',
        ];
    }
}
