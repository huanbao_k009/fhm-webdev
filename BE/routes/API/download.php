<?php
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API'], function () {
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::group(['prefix' => 'download', 'namespace' => 'Download'], function () {
            Route::get('songs', 'SongController@show');
            Route::get('album/{album}', 'AlbumController@show');
            Route::get('artist/{artist}', 'ArtistController@show');
            Route::get('playlist/{playlist}', 'PlaylistController@show');
            Route::get('favorites', 'FavoritesController@show');
        });
    });
});
