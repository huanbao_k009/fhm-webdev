<?php
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API'], function () {
    Route::group(['middleware' => 'os.auth', 'prefix' => 'os', 'namespace' => 'ObjectStorage'], function () {
        Route::group(['prefix' => 's3', 'namespace' => 'S3'], function () {
            Route::post('song', 'SongController@put')->name('s3.song.put'); // we follow AWS's convention here.
            Route::delete('song', 'SongController@remove')->name('s3.song.remove'); // and here.
        });
    });
});
