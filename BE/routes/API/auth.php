<?php
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API'], function () {
    Route::post('signin', 'AuthController@signin')->name('auth.signin');
    Route::post('forgot_pass', 'AuthController@forgot_pass')->name('auth.forgot_pass');
    Route::post('me', 'AuthController@login')->name('auth.login');
    Route::get('reset/{hash}/{rd}', 'AuthController@reset_pass')->name('auth.reset');
    Route::get('active/{hash}', 'AuthController@active')->name('auth.reset');
    Route::delete('me', 'AuthController@logout');
});
