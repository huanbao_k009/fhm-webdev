<?php
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API'], function () {
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::resource('user', 'UserController', ['only' => ['store', 'update', 'destroy']]);
        Route::get('me', 'ProfileController@show');
        Route::put('me', 'ProfileController@update');
        Route::post('image/upload',  'ProfileController@imageUpload');
    });
});
