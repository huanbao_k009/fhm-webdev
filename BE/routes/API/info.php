<?php
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API'], function () {
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::group(['namespace' => 'MediaInformation'], function () {
            Route::get('album/{album}/info', 'AlbumController@show');
            Route::get('artist/{artist}/info', 'ArtistController@show');
            Route::get('{song}/info', 'SongController@show')->name('song.show.deprecated'); // backward compat
            Route::get('song/{song}/info', 'SongController@show');
        });

    });
});
