<?php

use Illuminate\Support\Facades\Route;

require base_path('routes/API/auth.php');
require base_path('routes/API/broadcasting.php');
require base_path('routes/API/download.php');
require base_path('routes/API/info.php');
require base_path('routes/API/interaction.php');
require base_path('routes/API/lastfm.php');
require base_path('routes/API/playlist.php');
require base_path('routes/API/s3.php');
require base_path('routes/API/song.php');
require base_path('routes/API/user.php');

//Other API
Route::group(['namespace' => 'API'], function () {
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('/ping', function () {});

        Route::get('data', 'DataController@index');

        Route::post('settings', 'SettingController@store');
    });
});
