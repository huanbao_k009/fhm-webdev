@component('mail::message')
# Hi there !

Someone request a new password for your account on FHM 

@component('mail::button', ['url' => $url])
Reset Password
@endcomponent

If you didn't make this request then you can safely ignore this email =]] 

Thanks,<br>

@endcomponent
